import React, { Component } from 'react';
import { createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import HomeComponent from './src/components/Home/Home'
import SignupButton from './src/components/Signup/Signup'
import ProfilePic from './src/components/ProfilePic/ProfilePic'
import HomeProfile from './src/components/Profile/HomeProfile'
import Login from './src/components/Login/Login'
import ErrorComponent from './src/components/Error/Error'
import ProfileDetails from './src/components/Profile/ProfileDetails'
import HomeTab from './src/components/MainScreen/AppTabNavigator/HomeTab'
import ChatTab from './src/components/MainScreen/AppTabNavigator/ChatTab'
import PeopleTab from './src/components/MainScreen/AppTabNavigator/PeopleTab'
import DietTab from './src/components/MainScreen/AppTabNavigator/DietTab'
import MessagesTab from './src/components/MainScreen/AppTabNavigator/MessagesTab'
import { Icon, Item, Input, Thumbnail } from 'native-base'
import { Image } from 'react-native'
import Post from './src/modules/post/index'

import {
  View,
  Text,
  StyleSheet,
  Platform, Button
} from "react-native";

const tintColor = 'white';
const MainNavigator = createStackNavigator({
  Home: {
    screen: HomeComponent,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  Signup: {
    screen: SignupButton,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  ProfilePic: {
    screen: ProfilePic,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  HomeProfile: {
    screen: HomeProfile,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  Login: {
    screen: Login,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  ProfileDetails: {
    screen: ProfileDetails,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  Error: {
    screen: ErrorComponent,
    navigationOptions:
    {
      gestureEnabled:false,
      header: null,
    }
  },
  Post: {
    screen: Post,
  },
  MainScreen: {
    screen: createBottomTabNavigator({

      HomeTab: {
        screen: HomeTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-home" style={{ color: tintColor }} />
          )
        }


      },
      ChatTab: {
        screen: ChatTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-chatboxes" style={{ color: tintColor }} />
          )
        }

      },
      PeopleTab: {
        screen: PeopleTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="person" style={{ color: tintColor }} />
          )
        }

      },
      DietTab: {
        screen: DietTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-nutrition" style={{ color: tintColor }} />
          )
        }

      },
      MessagesTab: {
        screen: MessagesTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-text" style={{ color: tintColor }} />
          )
        }

      }

    }, {

        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: "bottom",
        tabBarOptions: {
          style: {
            ...Platform.select({
              android: {
                backgroundColor: 'white'
              }
            })
          },
          activeTintColor: '#29BEFF',
          inactiveTintColor: '#e0e0e0',
          showLabel: false,
          showIcon: true
        }
      },
    ),
    navigationOptions: {
      headerStyle: { backgroundColor: '#29BEFF' },
      headerLeft: <Item style={{borderColor:'transparent'}}>
        <Image style={{ width: 20, height: 20, marginLeft: 15 }} source={require('./assets/icons/logo_g1.png')} />

        <Item style={{ flex: 1, alignItem: 'center', width:'150%',height:40, backgroundColor: 'white', marginLeft: 20, padding: 5, marginBottom: 5, marginTop: 5, height: 40, borderRadius: 15 }}>
          <Icon style={{marginLeft: 10 }} color='#f1f3f4' name="search" type='EvilIcons' />
          <Input inputTextStyle={{ marginLeft:0,color: '#f1f3f4' }} style={{ width: 200,height:50, fontSize: 14 }} placeholder="Search for people and gyms " />
        </Item>

      </Item>,
      headerRight: <Icon style={{ marginRight: 10 }} type='MaterialIcons' name='notifications-active' style={{ color: 'white', marginRight: 12 }} />

    },

  },

});


const App = createAppContainer(MainNavigator);

export default App;
