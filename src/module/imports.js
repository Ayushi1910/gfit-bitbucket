import AuthLogin from './auth/login/index';
import ProfileInfo from './app/profileInfo/index';
import Login from './auth/login/login';
import Signup from './auth/signup/index';
import ProfilePic from './auth/profilePic/index';
// import ErrorScreen from './src/components/Error/Error'
import ProfileDetails from './app/profileInfo/profiledetails'
import HomeTab from './app/tabs/hometab/index'
import ChatTab from './app/tabs/chattab/index'
import PeopleTab from './app/tabs/peopletab/index'
import DietTab from './app/tabs/diettab/index'
import MessageTab from './app/tabs/messagetab/index'
import Post from '../module/app/post'
export { AuthLogin, ProfileInfo, Login, Signup, ProfilePic,ProfileDetails,HomeTab,ChatTab,MessageTab,DietTab,PeopleTab,Post }