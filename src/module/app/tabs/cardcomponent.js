import React, { Component } from "react";
import {
   View,
   Text,
   StyleSheet,
   Image,
   ScrollView
} from "react-native";

import { Card, CardItem, Thumbnail, Body, Left, Right, Button, Icon} from 'native-base'

class CardComponent extends Component {

   render() {

       return (
        <ScrollView
        scrollEnabled={true}>
           <Card style={{elevation:0,borderWidth:0,borderTopColor:'transparent',marginTop:0,padding:0}}>
               <CardItem>
                   <Left>
                       <Thumbnail source={{uri: this.props.userpic}} />
                       <Body>
                           <Text>{this.props.name} </Text>
                           <Text note>Jan 15, 2018</Text>
                       </Body>
                   </Left>
               </CardItem>
               <CardItem>
                   <Thumbnail small source={{uri: this.props.images}}  style={{ height: 200, width: null, flex: 1 }} />
              </CardItem>
               <CardItem style={{ height: 45 }}>
                   <Left>
                       <Button transparent>
                           <Icon name="ios-heart-outline" style={{ fontSize:24,color: 'black' }}/>
                       </Button>
                       <Button transparent>
                           <Icon name="ios-chatbubbles-outline" style={{ fontSize:24,color: 'black' }} />
                       </Button>
                       <Button transparent>
                           <Icon name="ios-send-outline" style={{fontSize:24, color: 'black' }} />
                       </Button>
                   </Left>
               </CardItem>

               {/* <CardItem style={{alignSelf:'baseline'}}>
                   <Text>{this.props.like}</Text>
               </CardItem> */}
               <CardItem>
                   <Body>
                       <Text>
                           <Text style={{ fontWeight: "700" }}>
                           </Text> 
                        {this.props.text}
                            </Text>
                   </Body>
               </CardItem>
           </Card>
           </ScrollView>
       );
   }
}
export default CardComponent;

const styles = StyleSheet.create({
   container: {
       flex: 1,
       alignItems: 'center',
       justifyContent: 'center'
   }
});