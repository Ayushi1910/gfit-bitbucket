import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";

import { Icon } from 'native-base'


class PeopleTab extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>PeopleTab</Text>
            </View>
        );
    }
}
export default PeopleTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})