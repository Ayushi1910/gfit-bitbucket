import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";

import { Icon } from 'native-base'


class MessageTab extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>MessageTab</Text>
            </View>
        );
    }
}
export default MessageTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})