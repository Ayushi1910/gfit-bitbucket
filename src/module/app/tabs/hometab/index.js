import React, { Component } from 'react';
import { Modal, View, Text, TouchableOpacity, TouchableHighlight, StyleSheet, AsyncStorage, Image, TextInput, ScrollView } from 'react-native';
import { SearchBar } from 'react-native-elements'
import { Container, Content, Segment, Header, Thumbnail, Item, Button, Icon, Left, Right, Input } from 'native-base';
import CardComponent from '../cardcomponent'
const SEGMENT = {
  TIMELINE: 'Timeline',
  FEEDS: 'Article'
}

const COLORS = {
  DARK_BLUE: 'black',
  WHITE: '#FFF'
}


class HomeTab extends Component {
  state = { activeSegment: 'Timeline', showFeed: false, modalVisible: false, }
  feeds = {}

  constructor(props) {
    super(props)

    this.state = {
      count: 0,
      url: [],
      users: [],
      userpic: [],
      name: [],
      text: [],
      likes: [],
      comments: {},
      images: {}
    }
    this.getFeedData = this.getFeedData.bind(this);

  }
  componentDidMount() {
    this.getFeedData()
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  getFeedData() {

    this.state.url = 'http://18.207.222.240:8000/feed/show_feeds/'
    fetch(this.state.url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Token ' + this.props.navigation.state.params.token

      },
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // dd = []
        // response.json.forEach(res => {
        //   user={}
        //   user['name'] = res['user']['name']
        //   user['user_pic'] = res['user']['profile_url']
        //   user['text'] = res['text']
        //   user['media_url'] = res['media_url']

        //   dd.push(user)
        // });
        // console.log("---=-=-=-=-====",)
        data = JSON.parse(JSON.stringify(responseJson))

        this.setState({ count: data.count })
        this.setState({ name: data.results.map((ele, i) => ele.user.name) })
        this.setState({ userpic: data.results.map((ele, i) => ele.user.profile_url) })
        this.setState({ text: data.results.map((ele, i) => ele.text) })
        this.setState({ images: data.results.map((ele, i) => ele.media_url) })

      })
      .catch((error) => {
        alert(error)
      });

  }
  render() {
    const { count,
      userpic,
      name,
      text,
      likes,
      comments,
      images
    } = this.state


    var feeds = []
    for (let i = 0; i < this.state.count; i++) {

      feeds.push(
        <CardComponent
          images={this.state.images[i]}
          userpic={this.state.userpic[i]}
          text={this.state.text[i]}
          name={this.state.name[i]}
          like="121 likes" />
      )
    }

    return (

      <Container style={styles.container}>
        <ScrollView
          scrollEnabled={true}>
          {/* <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              { alert('Modal has been closed.'); }
            }}>
            <View style={{ marginTop: 22 }}>
              <View>
                <Text>Profile</Text>

                <TouchableOpacity
                  onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                  }}>
                  <Text>Hide Modal</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal> */}

          <Item style={{ backgroundColor: '#f1f3f4', height: 50, paddingBottom: 10, paddingTop: 10, borderWidth: 0, borderRadius: 0 }}>
            <TouchableOpacity  >
              <Thumbnail small style={{ marginLeft: 12, marginRight: 15 }} source={{ uri: 'http://img2.thejournal.ie/inline/2470754/original?width=428&version=2470754' }} />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => { this.props.navigation.navigate('Post') }}
              style={{ color: '#f1f3f4', height: 40, width: '70%', marginRight: 12, backgroundColor: 'white', borderRadius: 8 }} >
              <TextInput
                editable={false}
                style={{ padding: 10, justifyContent: 'center' }}
                placeholder="What's happening?" />

            </TouchableOpacity>
            <Icon onPress={() => { this.props.navigation.navigate('Post') }}style={{ marginLeft: 10, fontSize: 24, marginRight: 5 }} name="camera" color='#f5f5f5' />
          </Item>

          <Segment style={{ borderRadius: 8, marginHorizontal: 6, height: 30, marginTop: 8, marginBottom: 8 }}>
            <Button enabled={true} style={{ flex: 1, justifyContent: 'center', borderColor: COLORS.DARK_BLUE, borderWidth: 1, backgroundColor: this.state.activeSegment == SEGMENT.TIMELINE ? COLORS.DARK_BLUE : COLORS.WHITE, borderTopLeftRadius: 8, borderBottomLeftRadius: 8 }} first onPress={() => this.setState({ activeSegment: SEGMENT.TIMELINE })} >
              <Text style={{ color: this.state.activeSegment == SEGMENT.TIMELINE ? COLORS.WHITE : COLORS.DARK_BLUE }} >{SEGMENT.TIMELINE}</Text>
            </Button>
            <Button style={{ flex: 1, justifyContent: 'center', borderColor: COLORS.DARK_BLUE, borderWidth: 1, backgroundColor: this.state.activeSegment == SEGMENT.FEEDS ? COLORS.DARK_BLUE : COLORS.WHITE, borderTopRightRadius: 8, borderBottomRightRadius: 8 }} onPress={() => this.setState({ activeSegment: SEGMENT.FEEDS })}>
              <Text style={{ textAlign: 'center', color: this.state.activeSegment == SEGMENT.FEEDS ? COLORS.WHITE : COLORS.DARK_BLUE }}>{SEGMENT.FEEDS}</Text>
            </Button>
          </Segment>


          <Content>

            {feeds}

          </Content>
        </ScrollView>



      </Container>
      //       <View>
      //         <Header
      //   placement="center"
      //   leftComponent={{ icon: 'arrow-back', color: '#fff' }}
      //   centerComponent={
      //     <SearchBar
      //     searchIcon={{ size: 16}}
      //     inputContainerStyle={{width:280,backgroundColor:'white',height:20}}
      //     containerStyle={{width:300,borderRadius:20,backgroundColor:'white'}}
      //   placeholder='Search' />
      //   }
      //   rightComponent={{ icon: 'notifications', color: '#fff' }}
      // />
      //       </View>
    );

  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  header: {
    backgroundColor: '#29BEFF'
  },
  titleText: {
    color: '#29BEFF'
  },
  tabsegment: {
    backgroundColor: 'white'
  }

});
export default HomeTab;
