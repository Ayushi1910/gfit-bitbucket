import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";

import { Icon } from 'native-base'


class ChatTab extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>ChatTab</Text>
            </View>
        );
    }
}
export default ChatTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})