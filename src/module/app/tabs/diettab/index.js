import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet
} from "react-native";

import { Icon } from 'native-base'


class DietTab extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text>DietTab</Text>
            </View>
        );
    }
}
export default DietTab;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    }
})