import React from 'react';
import { View, TouchableOpacity, Text, TextInput } from 'react-native';
import AntDesignIcon from 'react-native-vector-icons/AntDesign';
import { Avatar } from '../../common/component/index';
import ImagePicker from 'react-native-image-crop-picker';
import { Icon,Thumbnail,Picker } from 'native-base'

const BLUE_COLOR = '#29BEFF';

class Post extends React.Component {
    state = {
        privacy: 'public',
        post: '',
    }

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state
        return {
            headerTitle: (<Text style={{ flex: 1, color: '#FFF', textAlign: 'center' }}> Create Post </Text>),
            headerRight: <TouchableOpacity style={{ marginRight: 15 }} onPress={params.handlePost}>
                <AntDesignIcon name="check" />
            </TouchableOpacity>,
            headerStyle: { backgroundColor: BLUE_COLOR }
        }
    }

    handlePost = () => {
        alert("post button")
    }

    componentDidMount() {
        this.props.navigation.setParams({ handlePost: this.handlePost })
    }


    handleGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            const file = { uri: image.path, name: 'photo', type: 'image/jpeg' };
            alert("file to be upload", file)
        })
            .catch(error => console.log("error", error))
    }

    render() {
        return (
            <View style={{flex:1,rpaddingHorizontal: 15 }}>
                <View style={{ height: 100, flexDirection: 'row', marginTop: 10 }}>
                    <View style={{ flex: 2 }}>
                    <Thumbnail style={{ width: 50, height: 50,marginTop:12, marginLeft: 12, marginRight: 15 }} source={{uri:'http://img2.thejournal.ie/inline/2470754/original?width=428&version=2470754'}} />
                    </View>

                    <View style={{flex:8 }}>
                        
                    <Text style={{marginTop:13}}>Name</Text>
                            <View style={{ flexDirection: 'row'}}>
                            
                            <Icon name='people' style={{marginTop:12,fontSize:24, padding:0, alignItems:'center'}}/>
                                <Picker iosHeader="Select one"
                                    
                                    style={{ width:110}}
                                    mode="dropdown"
                                    selectedValue={this.state.privacy}

                                    onValueChange={(itemValue, itemIndex) => this.setState({ privacy: itemValue })}>

                                    <Picker.Item label="Private" value="private" />
                                    <Picker.Item label="Public" value="public" />

                                </Picker>
                            </View>
                           

                    </View>
                </View>
                <View style={{ height: 200, borderColor: '#3333', borderWidth: 1,marginHorizontal:18, borderRadius:8 }}>
                    <TextInput
                       style={{padding:8}}
                        multiline={true}
                        numberOfLines={8}
                        placeholder="what's on your mind ?"
                        onChange={(text) => console.log("text change")}
                    />
                </View>
                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                    <Text style={{ flex: 1 ,marginHorizontal:18}}> Attachment </Text>
                    <TouchableOpacity onPress={this.handleGallery} style={{ marginHorizontal:18,borderRadius:8,flex: 1, backgroundColor: '#53B653', height: 40, width: 60, justifyContent: 'center', alignItems: 'center' }}>
                        <Text> Browser Photo </Text>
                    </TouchableOpacity>
                </View>

            </View>
        );
    }
}

export default Post;
