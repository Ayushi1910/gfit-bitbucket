import React, { Component } from 'react'
import {
  Text,
  View,ActivityIndicator,Picker,StyleSheet,TouchableOpacity,Keyboard
} from 'react-native'
import { Input } from 'react-native-elements'
import RNPickerSelect from 'react-native-picker-select'

import LoadingSpinner from '../../../common/service/LoadingSpinner'

export default class ProfileDetails extends Component {
  constructor(props) {
    super(props)

    this.state = {
        
      height:'',
      weight:'',
      worksAt:'',
      loaded:false,
      interests:'',
      status:'',
      gender:'',
      items_interests: [
        {
            label: 'Woman',
            value: 'women',
        },
        {
            label: 'Man',
            value: 'men',
        },
        {
            label: 'Both',
            value: 'men_women',
        },
        {
          label: 'None',
          value: 'none',
      },
    ],
    items_status: [
      {
          label: 'Single',
          value: 'single',
      },
      {
          label: 'Committed',
          value: 'committed',
      },
      {
          label: 'Married',
          value: 'married',
      },
      {
        label: 'None',
        value: 'none',
    },
  ],
  items_gender: [
    {
        label: 'Female',
        value: 'f',
    },
    {
        label: 'Male',
        value: 'm',
    },
    {
        label: 'Other',
        value: 'o',
    },
],
    }

    LoadingSpinner.loading(load => this.setState({loaded:true}))
    this.skip = this.skip.bind(this)
    this.handleKeyboardPress=this.handleKeyboardPress.bind(this)
    this.profiledetails = this.profiledetails.bind(this)

  }
  handleKeyboardPress(){
    Keyboard.dismiss();

  }
  profiledetails = async () => {

        fetch('http://18.207.222.240:8000/rest-auth/update_profile/', {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Token '+ this.props.navigation.state.params.token
            },
            body: JSON.stringify({
              
              "height":(this.state.height || this.props.navigation.state.params.height),
              "weight":(this.state.weight || this.props.navigation.state.params.weight),
              "works_at":(this.state.worksAt || this.props.navigation.state.params.worksAt),
              "interested_in":(this.state.interests || this.props.navigation.state.params.interests),
              "status":(this.state.status || this.props.navigation.state.params.status)
      
            })
          })
            .then((response) => response.json())
            .then((responseJson) => {
              data = JSON.parse(JSON.stringify(responseJson))
            
              if (data.result) {
                
                this.props.navigation.navigate('MainScreen',{
                                                      token:this.props.navigation.state.params.token,
                                                      name: this.props.navigation.state.params.name,
                                                      id: this.props.navigation.state.params.id,
                                                      profilepic :this.props.navigation.state.params.profilepic})
              }
              else if(data.error) {
                alert(data.error)
              }
            })
            .catch((error) => {
              console.error(error);
            });
    
}
skip(){
  this.props.navigation.navigate('MainScreen',{
    token:this.props.navigation.state.params.token,
    name: this.props.navigation.state.params.name,
    id: this.props.navigation.state.params.id,
    profilepic :this.props.navigation.state.params.profilepic})
}

  render() {
    const {
      height,
      weight,
      gender,
      worksAt,
      interests,
      status
    } = this.state

    return (
    <View style={styles.container}>
    <Text style={{marginTop:30,color: '#1059A4',
        fontSize: 16,
        justifyContent:'center'}}>Add Profile Details to get more followers</Text>
    <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Height</Text>
        <Input
        inputContainerStyle={styles.inputContainer}
        inputStyle={styles.inputStyle}
        autoFocus={false}
        autoCapitalize="none"
        value={height  || this.props.navigation.state.params.height || ''}
        onChangeText={height => this.setState({ height }) || this.props.navigation.state.params.height}
        returnKeyType="next"
        placeholder="in cm"
        onSubmitEditing={() => {
          this.handleKeyboardPress()
        }}>
        </Input>
        </View>
        <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Weight</Text>
        
        <Input
        inputContainerStyle={styles.inputContainer}
        inputStyle={styles.inputStyle}
        autoFocus={false}
        autoCapitalize="none"
        value={weight || this.props.navigation.state.params.weight}
        onChangeText={weight => this.setState({ weight }) || this.props.navigation.state.params.weight || ''}
        returnKeyType="next"
        placeholder="in lbs or kgs"
        onSubmitEditing={() => {
          this.handleKeyboardPress()
        }}>
        </Input>
        </View>
        <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Works At</Text>
        <Input
        inputContainerStyle={styles.inputContainer}
        inputStyle={styles.inputStyle}
        autoFocus={false}
        autoCapitalize="none"
        returnKeyType="next"
        value={worksAt || this.props.navigation.state.params.worksAt || ''} 
        onChangeText={worksAt => this.setState({ worksAt }) || this.props.navigation.state.params.worksAt}
        onSubmitEditing={() => {
          this.handleKeyboardPress()
        }}>
        </Input>
        </View>
    <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Interested in</Text>
    <RNPickerSelect
                    placeholder={{
                        label: 'Interestes In',
                        value: null,
                    }}
                    items={this.state.items_interests}
                    onValueChange={(value) => {
                        this.setState({
                            interests: value,
                         }
                        );
                    }}
                    style={styles.pickerSelectStyles }
                    value={this.state.interests || this.props.navigation.state.params.interests}
                />
    </View>
    <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Gender</Text>
    <RNPickerSelect
                    placeholder={{
                        label: 'Gender',
                        value: null,
                    }}
                    items={this.state.items_gender}
                    onValueChange={(value) => {
                        this.setState({
                            gender: value,
                        });
                    }}
                    style={styles.pickerSelectStyles }
                    value={this.state.gender}
                    
                />
    </View>
    <View style={styles.rowContainer}>
    <Text style={styles.titleText}>Relationship Status</Text>
    <RNPickerSelect
                    placeholder={{
                        label: 'Relationship status',
                        value: null,
                    }}
                    items={this.state.items_status}
                    onValueChange={(value) => {
                        this.setState({
                            status: value,
                        });
                    }}
                    style={styles.pickerSelectStyles}
                    value={this.state.status || this.props.navigation.state.params.status}
                    
                />
    </View>
  

    <TouchableOpacity style={styles.button}
          onPress={this.profiledetails}>
          <Text style={styles.buttonText}>Add Details</Text>

        </TouchableOpacity>
        <TouchableOpacity style={styles.skipbutton}
          onPress={this.skip}>
          <Text style={styles.buttonText}>Skip</Text>

        </TouchableOpacity>
   </View>
    );
  }
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    titleText: {
      flex:1,
      width:100,
        color: '#1059A4',
        fontSize: 11,
      },
    inputContainer: {
      marginTop:30,
      justifyContent: 'flex-end',
      height: 45,
      width:100,
      marginVertical: 15,
      marginHorizontal:35,
      paddingLeft:25
    },
    inputStyle: {
        flex: 1,
        marginLeft: 10,
        color: 'black',
        fontSize: 12,
      },
    button: {
        alignItems: 'center',
        fontSize:20,
        backgroundColor:'white',
        padding: 5,
        borderWidth: 1,
        borderColor: 'white',
        marginTop: 20,
        width: 300,
        backgroundColor:'#1059A4',
        justifyContent: 'center',
        borderRadius: 5,
        height: 45
      },
      skipbutton: {
        alignItems: 'center',
        fontSize:20,
        backgroundColor:'white',
        padding: 5,
        borderWidth: 1,
        borderColor: 'white',
        marginTop: 20,
        width: 150,
        backgroundColor:'#1059A4',
        justifyContent: 'center',
        borderRadius: 5,
        height: 45,
      },
      buttonText: {
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        color:'white'
      },
      rowContainer: {
        flex:1,
        height: 40,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingLeft: 16,
      },
     internalPickerContainer: {
       flex:1,
        width:  50,
        height:40,
        paddingLeft:30
      },
     pickerIosListItemContainer: {
        flex: 1,
        height: 40,
        backgroundColor:'#1059A4',
        color:'white',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius:10
      },
      pickerIosListItemText: {
        fontSize: 9,
      },
      pickerSelectStyles : {
        fontSize: 12,
        backgroundColor: 'white',
        color: 'black',
        width:300,
        justifyContent:'center',
        alignItems: 'center',
    },
});
