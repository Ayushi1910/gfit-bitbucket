import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Button } from 'react-native';
import { LoginButton, AccessToken } from 'react-native-fbsdk';
import { bold, red } from 'ansi-colors';

export default class FbLoginButton extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.fbButton}>
                <LoginButton
                    readPermissions={["public_profile", "email"]}
                    onLoginFinished={
                        (error, result) => {
                            if (error) {
                                alert("login has error: " + result.error);
                            } else if (result.isCancelled) {
                                alert("login is cancelled.");
                            } else {
                                AccessToken.getCurrentAccessToken().then(
                                    (data) => {
                                        let accessToken = data.accessToken;
                                        this.props.onFbLoginSuccess(accessToken.toString());
                                    })
                            }
                        }
                    }
                />
            </View>
        );
    }

};

const styles = StyleSheet.create({
    defaultButtonStyle: {

    },
    fbButton: {
        width: 300,
        height: 45,
        justifyContent: 'center',
        paddingLeft: 50,
        marginTop: 20,
        borderRadius: 5,
        backgroundColor: '#4267b2',
    }
});
