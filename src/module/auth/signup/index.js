import React, { Component } from 'react'
import {
    Alert,
    LayoutAnimation, TouchableOpacity, Dimensions, Image, UIManager,
    KeyboardAvoidingView, StyleSheet, ScrollView, Text, View,Keyboard
} from 'react-native'
import { Input, Button } from 'react-native-elements'
import LoadingSpinner from '../../../common/service/LoadingSpinner'
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import { signUp } from '../service'
import styles from './style';

// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
UIManager.setLayoutAnimationEnabledExperimental(true)

const CUSTOMER = 'https://s3.amazonaws.com/gfitimages/gymerblue.png'
const COACH = 'https://s3.amazonaws.com/gfitimages/trainer.png'

export default class Signup extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoading: false, selectedType: 'CUSTOMER',
            fontLoaded: false, name: '', email: '', password: '', city: '', confirmationPassword: '', emailValid: true,
            passwordValid: true, nameValid: true, cityValid: true, flag: false, url: '', loaded: false
        }
        LoadingSpinner.loading(load => this.setState({ loaded: true }))
    }

    signup = (async) => {
        LayoutAnimation.easeInEaseOut()
        // const nameValid = this.validateName()
        const passwordValid = this.validatePassword()
        // const cityValid = this.validateCity();
        const url = this.props.navigation.state.params.isFbLogin ? '/rest-auth/facebook_signup/' : 'rest-auth/signup/';
        const flag = this.props.navigation.state.params.isFbLogin ? true : passwordValid;

        if (flag) {
            const body = {
                "name": (this.state.name || this.props.navigation.state.params.name),
                "email": this.props.navigation.state.params.email,
                "password": this.state.password,
                "city": "New York",
                "role": this.state.selectedType
            }
            signUp(url, body)
                .then((data) => {
                    if (data.result) {
                        this.props.navigation.navigate('MainScreen', { token: data.result.token, profile_pic: data.result.profile_url })
                    }
                    else {
                        alert(data.error)
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        }
    }

    // validateEmail = () => {
    //     const { email } = this.state || this.props.navigation.state.params.email
    //     const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    //     const emailValid = re.test(email)
    //     this.setState({ emailValid })
    //     emailValid || this.emailInput.shake()
    //     return emailValid
    // }

    validateName = () => {
        const { name } = this.state || this.props.navigation.state.params.name
        const nameValid = name.length > 0
        LayoutAnimation.easeInEaseOut()
        this.setState({ nameValid })
        // nameValid || this.nameInput.shake()
        return nameValid
    }

    validatePassword = () => {
        const { password } = this.state
        const passwordValid = password.length >= 8
        LayoutAnimation.easeInEaseOut()
        this.setState({ passwordValid })
        return passwordValid
    }
    // validateCity = () => {
    //     const { city } = this.state
    //     const cityValid = (city.length > 0)
    //     LayoutAnimation.easeInEaseOut()
    //     this.setState({ cityValid })
    //     cityValid || this.cityInput.shake()
    //     return cityValid
    // }


    setSelectedType = selectedType =>
        this.setState({ selectedType })

    handleKeyboardPress(){
        Keyboard.dismiss();
        
        }

    render() {
        const {
            isLoading,
            selectedType,
            password,
            passwordValid,
            name,
            nameValid,
            city,
            cityValid,
            email,
            emailValid
        } = this.state

        return (
                <ScrollView
                    scrollEnabled={true}
                    keyboardShouldPersistTaps="handled"
                    contentContainerStyle={styles.container}
                >
                    <KeyboardAvoidingView
                        behavior="position"
                        contentContainerStyle={styles.formContainer}
                    >
                        <Text style={styles.signUpText}>Sign up</Text>
                        <Text style={styles.whoAreYouText}>WHO YOU ARE ?</Text>
                        <View style={styles.userTypesContainer}>
                            <UserTypeItem
                                label="CUSTOMER"
                                labelColor="#1059A4"
                                image={{uri : CUSTOMER}}
                                onPress={() => this.setSelectedType('customer')}
                                selected={selectedType === 'customer'}
                            />
                            <UserTypeItem
                                label="COACH"
                                labelColor="#1059A4"
                                image={{uri :COACH}}
                                onPress={() => this.setSelectedType('coach')}
                                selected={selectedType === 'coach'}
                            />
                        </View>
                        <View style={{width: '80%', alignItems: 'center'}}>
              <FormInput
                refInput={input => (this.nameInput = input)}
                icon="user"
                value={name || this.props.navigation.state.params.name}
                onChangeText={name => this.setState({ name }) || this.props.navigation.state.params.name}
                placeholder="Name"
                returnKeyType="next"
                errorMessage={nameValid ? null : 'Name required'}
                onSubmitEditing={() => {
                  this.validateName()
                  this.handleKeyboardPress()
                  this.emailInput.focus()
                }}
              />
              <FormInput
                refInput={input => (this.emailInput = input)}
                icon="envelope"
                value={email || this.props.navigation.state.params.email}
                onChangeText={email => this.setState({ email }) || this.props.navigation.state.params.email}
                placeholder="Email"
                keyboardType="email-address"
                returnKeyType="next"
                errorMessage={emailValid ? null : 'Please use the correct email'}
                onSubmitEditing={() => {
                  this.validateEmail()
                  this.handleKeyboardPress()
                  this.passwordInput.focus()
                }}
              />
              { !this.props.navigation.state.params.isFbLogin &&  <FormInput
                refInput={input => (this.passwordInput = input)}
                icon="lock"
                value={password}
                onChangeText={password => this.setState({ password })}
                placeholder="Password"
                secureTextEntry
                returnKeyType="next"
                errorMessage={passwordValid ? null : 'Please enter at least 8 characters'}
                onSubmitEditing={() => {
                  this.validatePassword()
                  this.handleKeyboardPress()
                }}
              />
              }
              {/* <FormInput
                refInput={input => (this.cityInput = input)}
                icon="location-pin"
                value={city}
                onChangeText={city =>
                  this.setState({ city })}
                placeholder="City"
                errorMessage={cityValid ? null : 'City cannot be empty'}
                returnKeyType="done"
                onSubmitEditing={() => {
                  this.validateCity()
                  this.handleKeyboardPress()
                }}
              /> */}
            </View>
            <View style={{marginTop:20,flex:-1}}>
            <Button
              title="NEXT"
              buttonStyle={styles.signUpButton}
              titleStyle={styles.signUpButtonText}
              onPress={this.signup}
            />
            </View>
            
          </KeyboardAvoidingView>


                </ScrollView>
                
        );
    }
}

export const UserTypeItem = props => {
    const { image, label, labelColor, selected, ...attributes } = props
    return (
        <TouchableOpacity {...attributes}>
            <View
                style={[
                    styles.userTypeItemContainer,
                    selected && styles.userTypeItemContainerSelected,
                ]}
            >
                <Text style={[styles.userTypeLabel, { color: labelColor }]}>
                    {label}
                </Text>
                <Image
                    source={image}
                    style={[
                        styles.userTypeMugshot,
                        selected && styles.userTypeMugshotSelected,
                    ]}
                />
            </View>
        </TouchableOpacity>
    )
}

export const FormInput = props => {
    const { icon, refInput, ...otherProps } = props
    return (
        <Input
            {...otherProps}
            ref={refInput}
            inputContainerStyle={styles.inputContainer}
            leftIcon={<Icon name={icon} color="#7384B4" size={18} />}
            inputStyle={styles.inputStyle}
            autoFocus={false}
            autoCapitalize="none"
            keyboardAppearance="dark"
            errorStyle={styles.errorInputStyle}
            autoCorrect={false}
            blurOnSubmit={false}
            placeholderTextColor="#7384B4"
        />
    )
}
