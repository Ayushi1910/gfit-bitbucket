import { StyleSheet, Dimensions, UIManager } from 'react-native';


UIManager.setLayoutAnimationEnabledExperimental &&
UIManager.setLayoutAnimationEnabledExperimental(true) 

const SCREEN_WIDTH = Dimensions.get('window').width
const SCREEN_HEIGHT = Dimensions.get('window').height

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 20,
    paddingTop: 20,
    backgroundColor: 'white',
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  formContainer: {
    flex: 1,
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  signUpText: {
    color: '#1059A4',
    fontSize: 28,
  },
  whoAreYouText: {
    color: '#1059A4',
    fontSize: 16,
  },
  userTypesContainer: {
    backgroundColor:'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: SCREEN_WIDTH,
    alignItems: 'center',
  },
  userTypeItemContainer: {
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.7,
  },
  userTypeItemContainerSelected: {
    opacity: 1,
  },
  userTypeMugshot: {
    margin: 4,
    height: 50,
    width: 50,
  },
  userTypeMugshotSelected: {
    
    height: 70,
    width: 70,
  },
  inputContainer: {
    flex:-1,
    paddingLeft: 8,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: '#1059A4',
    height: 45,
    marginVertical: 10,
  },
  inputStyle: {
    flex: 1,
    marginLeft: 10,
    color: 'black',
    fontSize: 16,
  },
  errorInputStyle: {
    marginTop: 0,
    textAlign: 'center',
    color: '#F44336',
  },
  signUpButtonText: {
    fontSize: 13,
  },
  signUpButton: {
    marginTop:30,
    width: 250,
    borderRadius: 50,
    height: 45,
    backgroundColor:'#1059A4'
  }
})


export default styles;