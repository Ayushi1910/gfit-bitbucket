import httpService from '../../common/service/httpService';

export function isUserExist(body) {
    return httpService.post('rest-auth/check_user/',body)
}

export function fbloginService(body) {    
    return httpService.post('rest-auth/facebook_token/',body)
}

export function login(body) {
    return httpService.post('rest-auth/login/',body);    
}

export function signUp(url,body) {
    return httpService.post(url,body)
}