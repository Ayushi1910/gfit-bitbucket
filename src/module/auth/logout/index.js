import React, { Component } from 'react';
import { Button } from 'react-native-elements';

export default class Logout extends Component {
  constructor(props) {
    super(props)
  }
  logout = (async) => {
    fetch('http://18.207.222.240:8000/rest-auth/logout/', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + (this.props.navigation.state.params.token)
      }
    })
      .then((response) => response.json())
      .then((responseJson) => {
        response.status
        alert(response.status)
      })
      .catch((error) => {
        console.error(error);
      });


  }
  render() {
    return (
      <Button title="Logout" onPress={this.logout} />
    )
  }
}