import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:'#29BEFF',
    },
    titleText: {
      fontSize: 50,
      alignItems: 'center',
      justifyContent: 'center',
    },
    errorInputStyle: {
      marginTop: 0,
      textAlign: 'center',
      color: '#F44336',
    },
    inputContainer: {
      marginTop:50,
      paddingLeft: 8,
      borderRadius: 5,
      borderWidth: 1,
      borderColor:'white',
      justifyContent: 'center',
      height: 45,
      marginVertical: 15,
    },
    inputStyle: {
      flex: 1,
      marginLeft: 10,
      color: 'white',
      fontWeight: 'bold',
      fontSize: 16,
    },
    button: {
      alignItems: 'center',
      fontSize:20,
      backgroundColor:'white',
      padding: 5,
      borderWidth: 1,
      borderColor: 'white',
      marginTop: 20,
      width: 300,
      
      justifyContent: 'center',
      borderRadius: 5,
      height: 45
    },
    buttonText: {
      alignItems: 'center',
      justifyContent: 'center',
      fontWeight: 'bold',
      color:'#1059A4'
    },
    input: {
      width: 200,
      fontSize: 20,
      height: 44,
      padding: 10,
      borderWidth: 1,
      marginVertical: 10,
    },
  });
  
export default styles;