import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image, AsyncStorage} from 'react-native';
import { Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import LoadingSpinner from '../../../common/service/LoadingSpinner';
import { login } from '../service';

export default class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      email: '',
      password:'',
      token:'',
      loaded:false
    }
    LoadingSpinner.loading(load => this.setState({loaded:true}))
    this.login = this.login.bind(this)
  }

  login = async () => {
      const body = {
        "email": this.state.email || this.props.navigation.state.params.email,
        "password": this.state.password
      }
      login(body)
      .then((data) => {
          console.log("login data",data)
          if (data.result) {
            AsyncStorage.setItem('token',data.result.token)
            // this.props.navigation.navigate('HomeProfile', { token: data.result.token})
            fetch('http://18.207.222.240:8000/rest-auth/profile?id='+data.result.id, {
              method: 'GET',
              headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Token ' + data.result.token
              }
            })
            .then((response) => response.json())
            .then((responseJson) => {
              profileresult=JSON.parse(JSON.stringify(responseJson)).result
              profile = JSON.parse(JSON.stringify(responseJson)).result.profile
              
              if (profile.height > 0 &&
                profile.weight > 0 &&
                profile.works_at > 0 &&
                profile.interested_in > 0 &&
                profile.status > 0) {
                  
                  this.props.navigation.navigate('MainScreen',{token:data.result.token,
                                                               name: data.result.name,
                                                               id: data.result.id,
                                                               profilepic :data.result.profile_url})
            
                  
              }
              else{
                
                this.props.navigation.navigate('ProfileDetails',
                                              { token: data.result.token,
                                                name: profileresult.name,
                                                id: profileresult.id,
                                                profilepic :profileresult.profile_url,
                                                height: profile.height,
                                                weight: profile.weight,
                                                worksAt:profile.works_at,
                                                interests:profile.interested_in,
                                                status: profile.status})
                
              }
              })
              .catch((error) => {
                console.error(error);
              });
        
          }
          else if(data.error) {
            alert(data.error)
          }
        })
        .catch((error) => {
          console.error(error);
        });
  }

  render() {
    const {
      email,
      password
    } = this.state

    return (
      <View style={styles.container}>
      <Image
        style={{
          justifyContent:'center',
          position:'relative',
          width:150,
          height:150,
          
        }}
        source={require('../../../../assets/icons/gfit_logo.png')}
      />
        <FormInput
          refInput={input => (this.emailInput = input)}
          icon="envelope"
          value={this.props.navigation.state.params.email}
          onChangeText={email => this.setState({ email }) || this.props.navigation.state.params.email}
          placeholder="Enter your email"
          keyboardType="email-address"
          returnKeyType="next"
        />
        <FormInput
                refInput={input => (this.passwordInput = input)}
                icon="lock"
                value={password}
                onChangeText={password => this.setState({ password })}
                placeholder="Password"
                secureTextEntry
                returnKeyType="next"
              />
        <TouchableOpacity style={styles.button}
          onPress={this.login}>
          <Text style={styles.buttonText}>Login</Text>

        </TouchableOpacity>

      </View>
   
    );
  }

}
export const FormInput = props => {
  const { icon, refInput, ...otherProps } = props
  return (
    <Input
      {...otherProps}
      ref={refInput}
      inputContainerStyle={styles.inputContainer}
      leftIcon={<Icon name={icon} color="white" size={18} />}
      inputStyle={styles.inputStyle}
      autoFocus={false}
      autoCapitalize="none"
      keyboardAppearance="dark"
      errorStyle={styles.errorInputStyle}
      autoCorrect={false}
      blurOnSubmit={false}
      placeholderTextColor="white"
    />
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor:'#29BEFF',
  },
  titleText: {
    fontSize: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorInputStyle: {
    marginTop: 0,
    textAlign: 'center',
    color: '#F44336',
  },
  inputContainer: {
    marginTop:20,
    paddingLeft: 8,
    borderRadius: 5,
    borderWidth: 1,
    borderColor:'white',
    justifyContent: 'center',
    height: 45,
    marginVertical: 25,
  },
  inputStyle: {
    flex: 1,
    marginLeft: 10,
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  button: {
    alignItems: 'center',
    fontSize:20,
    backgroundColor:'white',
    padding: 5,
    borderWidth: 1,
    borderColor: 'white',
    marginTop: 10,
    width: 300,
    
    justifyContent: 'center',
    borderRadius: 5,
    height: 45
  },
  buttonText: {
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    color:'#1059A4'
  },
  input: {
    width: 200,
    fontSize: 20,
    height: 44,
    padding: 10,
    borderWidth: 1,
    marginVertical: 10,
  },
});
