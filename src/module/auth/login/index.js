import React, { Component } from 'react';
import { AsyncStorage, Text, View, TouchableOpacity, Image } from 'react-native';
import FbLoginButton from '../fbButton'
import { Input } from 'react-native-elements'
import SplashScreen from 'react-native-splash-screen'
import Icon from 'react-native-vector-icons/SimpleLineIcons'
import LoadingSpinner from '../../../common/service/LoadingSpinner';
import { isUserExist, fbloginService, } from '../service';
import styles from './style';


export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      emailValid: true,
      checkUser: false,
      result: '',
      fbLoginSucess: false,
      accessToken: '',
      loaded: false
    }
    LoadingSpinner.loading(load => this.setState({ loaded: true }))
  }

  async componentWillMount() {
    SplashScreen.hide()
    const token = await AsyncStorage.getItem('token');
    if (token) {
      this.props.navigation.navigate('MainScreen', { token: token })
    }
  }

  checkUser = async () => {
    const emailValid = this.validateEmail()
    if (emailValid) {
      isUserExist({ "email": this.state.email })
        .then((responseJson) => {
          result = JSON.parse(JSON.stringify(responseJson)).result
          if (result) {
            this.props.navigation.navigate('Login', { email: this.state.email })
          } else {
            this.props.navigation.navigate('Signup', { email: this.state.email, isFbLogin: false })
          }
        })
        .catch((error) => {
          console.error(error);
        });
    }
    else { alert('Invalid') }
  }

  validateEmail = () => {
    const { email } = this.state
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    const emailValid = re.test(email)
    this.setState({ emailValid })
    emailValid || this.emailInput.shake()
    return emailValid
  }

  render() {
    const {
      email,
      emailValid
    } = this.state

    return (
        <View style={styles.container}>
          <Image
            style={{
              justifyContent:'center',
          position:'relative',
          width:150,
          height:150,

            }}
            source={require('../../../../assets/icons/gfit_logo.png')}
          />
          <FormInput
            refInput={input => (this.emailInput = input)}
            icon="envelope"
            value={email}
            onChangeText={email => this.setState({ email })}
            placeholder="Enter your email"
            keyboardType="email-address"
            returnKeyType="next"
            errorMessage={emailValid ? null : 'Please enter a valid email address'}
            onSubmitEditing={() => {
              this.validateEmail()
            }}
          />
          <TouchableOpacity style={styles.button}
            onPress={this.checkUser}>
            <Text style={styles.buttonText}> Continue with Email</Text>

          </TouchableOpacity>
          <FbLoginButton onFbLoginSuccess={this.handleFBLogin} />

        </View>
        
    );
  }

  fblogin() {
    fbloginService({ "access_token": this.state.accessToken })
      .then((data) => {
        if (data.result) {
          this.props.navigation.navigate('Signup', { email: data.result.email, name: data.result.name, isFbLogin: true })
        }
        else if (data.login) {
          this.props.navigation.navigate('HomeProfile', { token: data.login.token, isFbLogin: true })
        }
        else {
          alert(data.error)
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }


  handleFBLogin = (accessToken) => {
    this.state.accessToken = accessToken;
    this.setState({ fbLoginSucess: true })
    this.fblogin(accessToken)
  }
}


export const FormInput = props => {
  const { icon, refInput, ...otherProps } = props
  return (
    <Input
      {...otherProps}
      ref={refInput}
      inputContainerStyle={styles.inputContainer}
      leftIcon={<Icon name={icon} color="white" size={18} />}
      inputStyle={styles.inputStyle}
      autoFocus={false}
      autoCapitalize="none"
      keyboardAppearance="dark"
      errorStyle={styles.errorInputStyle}
      autoCorrect={false}
      blurOnSubmit={false}
      placeholderTextColor="white"
    />
  )
}
