import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Avatar, NativeBaseButton } from '../../../common/component/index';
import ImagePicker from 'react-native-image-crop-picker';

class ProfilePic extends Component {
    state = { openModal: false }

    handleCamera = () => {
        ImagePicker.openCamera({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            const file = { uri: image.path, name: 'photo', type: 'image/jpeg' };
            console.log("file to be upload",file)
        })
        .catch(error => console.log("error", error))
    }

    handleGallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true
        }).then(image => {
            const file = { uri: image.path, name: 'photo', type: 'image/jpeg' };
            console.log("file to be upload",file)
        })
        .catch(error => console.log("error", error))
    }

    handleNext = () => {
        alert('Profile Page!!!')

    }

    render() {
        const url = this.props.navigation.state.params.profile_pic;
        return (
            <View style={styles.container}>
            <Text style={{color:'white',fontSize:20,justifyContent:'center',marginTop:30}}>Add Your Profile picture</Text>
                <View style={styles.avatarContainer}>
                    <View style={{ flex: 2, justifyContent: 'center' }}>
                        <Avatar size={100} uri={url} />
                    </View>
                    {/* <View style={styles.optionContainer}> 
                        <NativeBaseButton label="Add Photo" />
                    </View>  */}
                    <PicOptions style={{backgroundColor:'white',color:'black'}} onCameraPress={this.handleCamera} onGalleryPress={this.handleGallery} />
                </View>
                <View style={styles.buttonContainer}>
                    <View style={{ flex: 1, alignSelf: 'center', justifyContent: 'center' }}>
                        <NativeBaseButton onPress={this.handleNext} title="Next" />
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignSelf: 'flex-end' }}>
                        <NativeBaseButton style={{ marginRight: 16 }} onPress={this.handleNext} title="Skip" />
                    </View>
                </View>
            </View>
        );
    }
}


const PicOptions = (props) => {
    return (
        <View style={styles.optionContainer}>
            <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => props.onCameraPress()}>
                <Text> Camera </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ justifyContent: 'center' }} onPress={() => props.onGalleryPress()}>
                <Text> Gallery </Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: { flex: 1,backgroundColor:'#29BEFF' },
    avatarContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonContainer: {
        flex: 1
    },
    optionContainer: { flex: 1, marginVertical: 5, flexDirection: 'row' }
})


export default ProfilePic;