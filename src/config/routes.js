import { createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import React, { Component } from "react";
import {
  AuthLogin, ProfileInfo, Login,
  Signup, ProfilePic, ProfileDetails,
  ErrorScreen, HomeTab,
  ChatTab, MessageTab, DietTab, PeopleTab, Post
} from '../module/imports';
import { Icon, Item, Input, Thumbnail } from 'native-base'
import { Image,Platform } from 'react-native'


const nullHeader = { header: null };
const MainNavigator = createStackNavigator({
  Home: { screen: AuthLogin, navigationOptions: { ...nullHeader } },
  Signup: { screen: Signup, navigationOptions: { ...nullHeader } },
  ProfilePic: { screen: ProfilePic, navigationOptions: { ...nullHeader } },
  HomeProfile: { screen: ProfileInfo, navigationOptions: { ...nullHeader } },
  Login: { screen: Login, navigationOptions: { ...nullHeader } },
  ProfileDetails: { screen: ProfileDetails, navigationOptions: { ...nullHeader } },
  Post: { screen: Post },
  MainScreen: {
    screen: createBottomTabNavigator({
      HomeTab: {
        screen: HomeTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (<Icon name="ios-home" style={{ color: tintColor }} />)
        }
      },
      ChatTab: {
        screen: ChatTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-chatboxes" style={{ color: tintColor }} />
          )
        }

      },
      PeopleTab: {
        screen: PeopleTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="person" style={{ color: tintColor }} />
          )
        }

      },
      DietTab: {
        screen: DietTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-nutrition" style={{ color: tintColor }} />
          )
        }

      },
      MessageTab: {
        screen: MessageTab,
        navigationOptions: {
          tabBarIcon: ({ tintColor }) => (
            <Icon name="ios-text" style={{ color: tintColor }} />
          )
        }

      }

    }, {
        animationEnabled: true,
        swipeEnabled: true,
        tabBarPosition: "bottom",
        tabBarOptions: {
          style: {
            ...Platform.select({
              android: {
                backgroundColor: 'white'
              }
            })
          },
          activeTintColor: '#29BEFF',
          inactiveTintColor: '#e0e0e0',
          showLabel: false,
          showIcon: true
        }
      },
    ),
    navigationOptions: {
      headerStyle: { backgroundColor: '#29BEFF' },
      headerLeft: <Item style={{ borderColor: 'transparent' }}>
        <Image style={{ width: 20, height: 20, marginLeft: 15 }} source={require('../../assets/icons/logo_g1.png')} />

        <Item style={{ flex: 1, alignItem: 'center', width: '150%', height: 40, backgroundColor: 'white', marginLeft: 20, padding: 5, marginBottom: 5, marginTop: 5, height: 40, borderRadius: 15 }}>
          <Icon style={{ marginLeft: 10 }} color='#f1f3f4' name="search" type='EvilIcons' />
          <Input inputTextStyle={{ marginLeft: 0, color: '#f1f3f4' }} style={{ width: 200, height: 50, fontSize: 14 }} placeholder="Search for people and gyms " />
        </Item>

      </Item>,
      headerRight: <Icon style={{ marginRight: 10 }} type='MaterialIcons' name='notifications-active' style={{ color: 'white', marginRight: 12 }} />

    },

  },

});

const App = createAppContainer(MainNavigator);

export default App;
