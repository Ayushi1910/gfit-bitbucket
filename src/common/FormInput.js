import { StyleSheet } from'react-native';
import React from 'react';
import { Input } from 'react-native-elements'
import Icon from 'react-native-vector-icons/SimpleLineIcons'

export const FormInput = props => {
    const { icon, refInput, ...otherProps } = props
    return (
      <Input
        {...otherProps}
        ref={refInput}
        inputContainerStyle={styles.inputContainer}
        leftIcon={<Icon name={icon} color="#7384B4" size={18} />}
        inputStyle={styles.inputStyle}
        autoFocus={false}
        autoCapitalize="none"
        keyboardAppearance="dark"
        errorStyle={styles.errorInputStyle}
        autoCorrect={false}
        blurOnSubmit={false}
        placeholderTextColor="#7384B4"
      />
    )
  }
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'white',
      alignItems: 'center',
      justifyContent: 'center',
    },
    titleText: {
      fontSize: 50,
      alignItems: 'center',
      justifyContent: 'center',
    },
    errorInputStyle: {
      marginTop: 0,
      textAlign: 'center',
      color: '#F44336',
    },
    button: {
      alignItems: 'center',
      backgroundColor: 'white',
      width: 200,
      height: 35,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      borderRadius: 5,
      marginBottom: 10,
      marginTop: 20
    },
    buttonText: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    input: {
      width: 200,
      fontSize: 20,
      height: 44,
      padding: 10,
      borderWidth: 1,
      borderColor: 'black',
      marginVertical: 10,
    },
    linearGradient: {
      width: 250,
      borderRadius: 50,
      height: 45,
    },
  });
  