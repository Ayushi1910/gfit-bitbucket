import { AsyncStorage } from 'react-native';

let ONLINE_STATUS = undefined;

const BASE_URL = 'http://18.207.222.240:8000/';
    
let obj = {
    get (url) {
        const METHOD = 'get';
        return doHttpCall(url, METHOD);
    },
    post (url, body) {
        const METHOD = 'post';
        return doHttpCall(url, METHOD, body);
    }
};

async function doHttpCall(url, method, body={}) {
    const NEW_URL = BASE_URL + url;
    const token = await AsyncStorage.getItem('token');

    const promise = new Promise((resolve, reject) => {
            let options = {
                method : method,
                headers :{
                    'Content-Type': 'application/json',
                    'token': token
                }
            };
            console.log("options",options);
            if (method === 'post') {
                options.body = JSON.stringify(body)
            }
            fetch(NEW_URL, options)
            .then(res=>res.json())
            .then((response) => {
               resolve(response)
            }).catch((message)=> {
                reject(message);
            });           
    });

    return promise;
}

export default obj;
