import React from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { Button } from 'react-native';

export const NativeBaseButton = (props) => <Button {...props} >
    <Text style={{ paddingHorizontal: 10, color: '#FFF' }} >{props.title}</Text>
</Button>