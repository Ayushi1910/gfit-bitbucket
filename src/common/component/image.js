import React from 'react';
import { Image } from 'react-native';

const defaultUser = 'https://b.kisscc0.com/20180817/plq/kisscc0-user-profile-computer-icons-facebook-avatar-ftkuser-5b7751e1c79415.8309983615345464018175.png';

export const Avatar = (props) => {
    const height = props.size || 50;
    const width = props.size || 50;
    const radius = height / 2;
    return <Image source={{ uri: props.uri || defaultUser }} style={{ height: height, width: width, borderRadius: radius }} />
}